	.file	"fab.c"
	.text
.globl main
	.type	main, @function
main:
	leal	4(%esp), %ecx
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ecx
	subl	$4, %esp
	movl	$5, (%esp)
	call	fac
	movl	%eax, (%esp)
	call	exit
	.size	main, .-main
