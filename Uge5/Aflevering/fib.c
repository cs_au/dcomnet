int main (void)
{
  exit(fib(5));
}

long fib (long n) 
{
  long c,k,l;
  
  if (n == 0)
    c = 0;
  else
  {
	if (n == 1)
	  c = 1;
	else
	  {
	    k = n-1;
		l = n-2;
		c = fib(k) + fib(l);
	  }
  }
  return c;
}
