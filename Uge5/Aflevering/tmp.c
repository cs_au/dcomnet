.section .text

.globl _start          
_start:                 # int main (void)
	pushl $5        
	call fac        	# fac(5)
	addl $4,%esp
    movl %eax,%ebx
	movl $1, %eax   	# return(fac(5))
	int $0x80       

.type fac, @function

fac:		            # long fac (long n)
    pushl %ebp          
	movl %esp,%ebp      
	subl $8,%esp        # long c,k
	movl 8(%ebp),%eax   
	movl $0,%ebx        
	cmpl %eax,%ebx      
    je if  	        	# if (n == 0)
    jmp else
if:
	movl $1,-4(%ebp)    # c = 1
	jmp endif	
else:
	decl %eax
	movl %eax,-8(%ebp)  # k = n - 1
	pushl %eax
	call fac            # fac(k)
	addl $4,%esp
	imull 8(%ebp),%eax  
	movl %eax,-4(%ebp)  # c = n*fac(k)
endif:
	movl -4(%ebp),%eax  # return value in %eax
	movl %ebp,%esp      # 
	popl %ebp           # 
    ret                 # return c