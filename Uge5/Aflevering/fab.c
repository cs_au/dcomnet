int main (void)
{
  exit(fac(5));
}

long fac (long n) 
{
  long c,k;

  if (n == 0)
    c = 1;
  else
    {
      k = n - 1;
      c = n * fac(k);
    }
  
  return c;
}
