
The archive

ijvm-tools-0.9-mac.zip

contains the files:

README.txt 
ijvm
ijvm-asm
mic1
mic1-asm
ijvm.spec
ijvm.mal

The programs are compiled from the source code in ijvm-tools-0.9 on Mac OSX Snow Leopard.

To install the tools, unzip the archive ijvm-tools-0.9-mac.zip in your home directory. Assuming that the archive is in your home directory, this can be done in a terminal window as:

cd ~
unzip ijvm-tools-0.9-mac.zip

This should create the directory ijvm-tools-0.9-mac in your home directory. To make the tools easy accessable in a terminal window, you should add the following lines to the file ~/.profile

export PATH=${PATH}:${HOME}/ijvm-tools-0.9-mac
export IJVM_SPEC_FILE=${HOME}/ijvm-tools-0.9-mac/ijvm.spec

The ijvm-tools should then be available when you open a new terminal.

\Christian Storm Pedersen <cstorm@cs.au.dk>, 13 Nov 2009.

