#include <stdio.h>

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		return 0;
	}
	
	int n = atoi(argv[0]);
	int i = atoi(argv[1]);
	int j = atoi(argv[2]);
}

void towers(int n, int i, int j)
{
	if (n == 1)
		printf("Move a disk from %d to %d", i, j);
	else
	{
		int k = 6 - i - j;
		
		towers(n - 1, i, k);
		towers(1, i, j);
		towers(n - 1, k, j);
	}
}