public class Towers
{
	public static void main(String[] args)
	{
		int n, i, j;
		n = Integer.parseInt(args[0]);
		i = Integer.parseInt(args[1]);
		j = Integer.parseInt(args[2]);
		
		towers(n, i, j);
	}
	
	public static void towers(int n, int i, int j)
	{
		int k;
		
		if (n == 1)
			System.out.println("Move a disk from " + i + " to " + j);
		else
		{
			k = 6 - i - j;
			towers(n - 1, i, k);
			towers(1, i, j);
			towers(n - 1, k, j);
		}
	}
}