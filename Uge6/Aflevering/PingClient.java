import java.io.*;
import java.net.*;
import java.util.Date;

public class PingClient
{	
	// Address variables.
	private static InetAddress serverIP;
	private static int serverPort;
	
	// Package variables.
	private static byte[] sendData = new byte[1024];
	private static byte[] receiveData = new byte[1024];
	
	// Misc variables.
	private static int pingRequests = 10;
	private static int pingDelay = 1000;
	private static int pingTimeout = 1000;
	private static final String CR = System.getProperty("line.separator");
	
	// Main method.
	public static void main(String[] args) throws Exception
	{
		/* Set up the IP and port number of the server, as
		 * provided by the arguments. If only one argument,
		 * assume it is a port and use localhost for IP. */		
		switch (args.length)
		{
			case 1:
				serverIP 	= InetAddress.getByName("localhost");
				serverPort 	= Integer.parseInt(args[0]);
				break;
			case 2:
				serverIP 	= InetAddress.getByName(args[0]);
				serverPort	= Integer.parseInt(args[1]);
				break;
			default:
				System.out.println("Required arguments: (host) port");
				return;
		}
		
		// Print information message.
		System.out.println("Pinging address: " + serverIP + ":" + serverPort
			+ " " + pingRequests + " times with a "
			+ pingTimeout + " ms timeout");
		
		/* Set up a socket for sending and receiving packets,
		 * and give it a timeout. */
		DatagramSocket clientSocket = new DatagramSocket();
		clientSocket.setSoTimeout(pingTimeout);
		
		/* Set up and send a specified amount of packets to
		 * the server, attempting to receive a reply-packet
		 * for each packet sent. */
		for (int i = 0; i < pingRequests; i++)
		{
			// Set up a new packet to send to the server.
			String msg = "PING " + i + " " + new Date() + CR;
			sendData = msg.getBytes();
			
			DatagramPacket sendPacket = new DatagramPacket(
				sendData, sendData.length, serverIP, serverPort);
			
			// Record the current time for approximating latency.
			long sendTime = System.currentTimeMillis();
			clientSocket.send(sendPacket);
			
			/* Attempt to receive a packet in the socket, and
			 * extract the data into the sentence-variable. The
			 * trim()-method on the new String removes any white
			 * space before and after the actual data.
			 * Prepare an error message if no packet is received
			 * within the pingTimeout time. */
			try
			{
				DatagramPacket receivePacket = new DatagramPacket(
					receiveData, receiveData.length);
				
				clientSocket.receive(receivePacket);
				
				// Approximate latency.
				long latency = System.currentTimeMillis() - sendTime;
				
				msg = new String(receivePacket.getData()).trim()
					+ " from " + receivePacket.getAddress()
					+ ", latency: " + latency + " ms";
			}
			catch(SocketTimeoutException e)
			{
				msg = "No reply from "	+ serverIP
					+ " within " + pingTimeout + " ms.";
			}
			
			// Print the success/fail message.
			System.out.println(msg);
			
			// Take a nap.
			Thread.sleep(pingDelay);
		}
	}
}